require('dotenv').config();
const express = require('express'),
  app = express(),
  bodyParser = require('body-parser');
const path = require('path');
app.use(express.static(__dirname + '/dist/elte-assistant'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
require('./server/appRoutes')(app);
app.get('/home', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/elte-assistant/index.html'));
});
app.get('/user', function (req, res) {
  res.json(req.user);
});
app.listen(process.env.PORT || 8080);

module.exports = app;
