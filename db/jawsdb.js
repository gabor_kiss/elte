'use strict';

const mysql = require('mysql');

const connection = mysql.createConnection({
  host: process.env["DB_JAWS_HOST"],
  user: process.env["DB_JAWS_USER"],
  password: process.env["DB_JAWS_PASSWORD"],
  database: process.env["DB_JAWS_DATABASE"]
});

connection.connect(function(err) {
  if (err) throw err;
});

module.exports = connection;
