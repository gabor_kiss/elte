const mysqlssh = require('mysql-ssh');

const connection = mysqlssh.connect(
  {
    host: process.env["DB_SSH_HOST"],
    user: process.env["DB_SSH_USER"],
    password: process.env["DB_SSH_PASSWORD"],
    port: 22
  },
  {
    host: process.env["DB_TTK_HOST"],
    user: process.env["DB_TTK_USER"],
    password: process.env["DB_TTK_PASSWORD"],
    database: process.env["DB_TTK_DATABASE"]
  }
);

module.exports = connection;
