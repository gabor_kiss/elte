const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');
chai.use(chaiHttp);
const should = chai.should();
const expect = chai.expect;

const courseResponseBody = [
  'kurzusnev','kurzuskod', 'idopont',
  'helyszin', 'hetek', 'megjegyzes',
  'oratipus', 'csoport', 'max_letszam',
  'eloadas', 'gyakorlat', 'oktato', 'nap', 'orakezdes'];

describe('list_all_majors', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/majors')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0){
          expect(Object.keys(res.body[0]) === ['id', 'name', 'degree', 'short_name', 'year']);
        }
        done();
      });
  });
});

describe('list_subjects_by_major_and_recommended', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/subjects/3/4')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) ===
            ['id', 'major_id', 'code',
              'name', 'lecture', 'exam',
              'exercise', 'exercise_mark', 'consultation',
              'credit', 'prerequisite1', 'prerequisite2',
              'recommended', 'required_optional']);
        }
        done();
      });
  });
});

describe('list_all_semesters', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/semesters')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) === ['tanev_felev']);
        }
        done();
      });
  });
});

describe('list_all_courses_by_name', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/courses/courseName/2019-2020-2/Adatbázisok')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) === courseResponseBody);
        }
        done();
      });
  });
});

describe('list_all_courses_by_code', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/courses/code/2019-2020-2/IP-08aAB1E')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) === courseResponseBody);
        }
        done();
      });
  });
});

describe('list_all_courses_by_teacher', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/courses/teacher/2019-2020-2/Visnovitz')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) === courseResponseBody);
        }
        done();
      });
  });
});

describe('list_all_courses_by_teacherCode', () => {
  it('should return with json', done => {
    chai
      .request(app)
      .get('/api/courses/teacherCode/2019-2020-2/WP1C0X')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.type === 'json');
        if(res.body.length > 0) {
          expect(Object.keys(res.body[0]) === courseResponseBody);
        }
        done();
      });
  });
});
