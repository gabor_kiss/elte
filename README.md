A to.ttk.elte.hu oldalon elérhető online tanrend adatbázisához egy olyan online felület készítése, mely a hallgatókat és az oktatókat az
alábbi módon segíti: 
* Felhasználóbarát (asztali és mobil) felületen kereshetők a tanrendben tárgyak, oktatók, megfelelő
szűrőbeállításokkal. 
* A rendszer támogasson Neptun vagy Caesar azonosítóval történő bejelentkezést, hogy a felhasználó saját
adatai mentésre kerülhessenek. 
* Az eredmények alapján az oktató látja a számára elkészített órarendet vizualizálva a könnyebb
áttekinthetőség érdekében. Ebben az különböző típusú (pl. "oktatói elfoglaltság", illetve a "0% adminisztrátor") időpontok másféle
stílussal jelenhetnek meg, az órarend megjelenése személyre szabható. 
* A hallgatónak legyen lehetősége
elmenteni az általa felvenni kívánt kurzusokat, melyek órarend formátumban megjelennek. Ez az órarend exportálható képként vagy
a legnépszerűbb naptár formátumokba. 
* Új tárgy felvétele esetén az új tárgyhoz tartozó kurzusok időpontjai kiemelve jelenjenek meg
az eddigi órarendben.