export const environment = {
  production: true,
  AUTH_REDIRECT_URI: 'https://elteassistant.herokuapp.com/'
};
