import {Component, OnInit} from '@angular/core';
import {MsAdalAngular6Service} from 'microsoft-adal-angular6';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpService} from '../service/http.service';
import moment from 'moment';

@Component({
  selector: 'app-bug-report',
  templateUrl: './bug-report.component.html',
  styleUrls: ['./bug-report.component.scss']
})
export class BugReportComponent implements OnInit {
  text = '';
  bugSent = true;

  constructor(
    private adalSvc: MsAdalAngular6Service,
    private snackBar: MatSnackBar,
    private httpService: HttpService
  ) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const bug = {name: this.adalSvc.userInfo.profile.name, userName: this.adalSvc.userInfo.userName, text: this.text, date: moment().format('YYYY-MM-DD HH:mm:ss')};
    console.log(bug);
    this.bugSent = false;
    this.httpService.postBug(bug).subscribe(
      data => {
        this.openSnackBar('Sikeresen elküldve', '');
        this.text = '';
      },
      error => {
        this.openSnackBar('Hiba történt küldés közben!', '');
      },
      () => {
        this.bugSent = true;
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

}
