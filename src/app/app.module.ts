import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {BugReportComponent} from './bug-report/bug-report.component';
import {AboutComponent} from './about/about.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {CurriculumMeshComponent} from './curriculum-mesh/curriculum-mesh.component';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {
  MatChipsModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import {CalendarComponent} from './calendar/calendar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {MatRadioModule} from '@angular/material/radio';
import {ListFilterPipe} from './calendar/table-filter.pipe';
import {AuthenticationGuard, MsAdalAngular6Module} from 'microsoft-adal-angular6';
import {environment} from '../environments/environment';
import {TokenInterceptor} from './service/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BugReportComponent,
    AboutComponent,
    CurriculumMeshComponent,
    CalendarComponent,
    ListFilterPipe
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSelectModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    FormsModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatInputModule,
    MatTooltipModule,
    MatButtonToggleModule,
    NgbDropdownModule,
    MatRadioModule,
    MsAdalAngular6Module.forRoot({
      tenant: 'ikelte.onmicrosoft.com',
      clientId: '3cfc3471-aaa2-4819-b2ac-d70f027a0abc',
      redirectUri: environment.AUTH_REDIRECT_URI,
      endpoints: {
        'https://localhost/Api/': '3cfc3471-aaa2-4819-b2ac-d70f027a0abc'
      },
      navigateToLoginRequestUrl: true,
      cacheLocation: 'localStorage',
    }),
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatChipsModule
  ],
  providers: [AuthenticationGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
