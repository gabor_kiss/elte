import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CurriculumMeshComponent} from './curriculum-mesh.component';
import {AppModule} from '../app.module';

describe('CurriculumMeshComponent', () => {
  let component: CurriculumMeshComponent;
  let fixture: ComponentFixture<CurriculumMeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumMeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
