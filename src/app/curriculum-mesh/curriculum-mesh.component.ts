import {Component, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';
import {Major, Subject} from '../service/Interfaces';
import {DefaultService} from '../service/default.service';

@Component({
  selector: 'app-curriculum-mesh',
  templateUrl: './curriculum-mesh.component.html',
  styleUrls: ['./curriculum-mesh.component.scss']
})
export class CurriculumMeshComponent implements OnInit {
  majors: Major[];
  semesters: string[] = ['1', '2', '3', '4', '5', '6'];
  selected: Selected = {majorId: 0, semester: 0};
  mandatorySubjects: Subject[] = [];
  optionalSubjects: Subject[] = [];

  constructor(
    public httpService: HttpService,
    public defaultService: DefaultService
  ) {
  }

  ngOnInit() {
    this.httpService.getAllMajors().subscribe(data => {
      this.majors = data;
    });
  }

  sendSelected(selected: Selected) {
    this.mandatorySubjects = [];
    this.optionalSubjects = [];
    this.httpService.getSubjectsByMajorIdAndYear(selected.majorId, selected.semester).subscribe( subjects => {
      console.log(subjects);
      subjects.forEach( subject => {
        if (subject.exercise_mark === 'GY') {
          subject.type = 'gyakorlat';
        } else if (subject.exam === 'K') {
          subject.type = 'előadás';
        } else {
          subject.type = 'összevont jegy';
        }

        if (subject.required_optional === 0) {
          this.mandatorySubjects.push(subject);
        } else {
          this.optionalSubjects.push(subject);
        }
      });
    });
  }

  getCreditSum(arr: any[]){
    var numbers = arr.map(i => i.credit);
    return numbers.reduce((a, b) => a + b, 0);
  }
}

interface Selected {
  majorId: number;
  semester: number;
}
