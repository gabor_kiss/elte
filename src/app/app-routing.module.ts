import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {BugReportComponent} from './bug-report/bug-report.component';
import {AboutComponent} from './about/about.component';
import {AuthenticationGuard} from 'microsoft-adal-angular6';

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'bug-report',
    component: BugReportComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'about',
    component: AboutComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
