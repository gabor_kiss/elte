import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {HttpService} from './service/http.service';
import {User} from './service/Interfaces';
import {MsAdalAngular6Service} from 'microsoft-adal-angular6';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'elte-assistant';
  view: string;

  constructor(
    private httpService: HttpService,
    private elementRef: ElementRef,
    public adalSvc: MsAdalAngular6Service
  ) {
    // this.adalSvc.RenewToken('https://graph.microsoft.com');
    // this.adalSvc.acquireToken('https://graph.microsoft.com').subscribe(token => {
    //   localStorage.setItem('authtoken', token);
    // });
  }

  ngOnInit(): void {
    this.httpService.getUser(this.adalSvc.userInfo.userName).subscribe(
      user => {
        if (user[0] === undefined) {
          const newUser: User = {
            name: this.adalSvc.userInfo.profile.name,
            userName: this.adalSvc.userInfo.userName,
            view: '1',
            timetable: '',
            lastChange: null
          };
          this.httpService.postUser(newUser).subscribe();
        }
      });
  }

  ngAfterViewInit(): void {
    if (this.adalSvc.isAuthenticated) {
      this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#e6e6e6';
    }
  }
}
