import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {ExportToCsv} from 'export-to-csv';
import * as ical from 'ical-generator';
import {Course} from '../../service/Interfaces';

@Injectable({
  providedIn: 'root'
})
export class ExportService {

  semesterStart = moment('09/09/2019', 'DD/MM/YYYY');
  semesterEnd = moment('14/12/2019', 'DD/MM/YYYY');

  constructor() {
  }

  generateCsv(courses: any[][]) {
    const courseArray = this.unboxCourses(courses);
    const semesterLength = this.semesterEnd.diff(this.semesterStart, 'week');
    const csvData = [];
    courseArray.forEach(course => {
      for (let i = 0; i < semesterLength + 1; ++i) {
        const startDate = moment(this.semesterStart.weekday(0));
        const endDate = moment(this.semesterStart.weekday(0));
        startDate.add(i, 'week');
        startDate.add(course.dayInd, 'day');
        endDate.add(i, 'week');
        endDate.add(course.dayInd, 'day');
        const data = {
          Subject: course.name,
          'Start Date': startDate.format('DD/MM/YYYY'),
          'Start Time': moment(course.hour.split('-')[0], 'hh:mm').format('LT'),
          'End date': endDate.format('DD/MM/YYYY'),
          'End Time': moment(course.hour.split('-')[1], 'hh:mm').format('LT'),
          Description: course.teacher,
          Location: course.location
        };
        csvData.push(data);
      }
    });
    const options = {
      fieldSeparator: ',',
      decimalSeparator: '.',
      showLabels: true,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(csvData);
  }

  generateIcs(courses: any[][]) {
    const courseArray = this.unboxCourses(courses);
    const semesterLength = this.semesterEnd.diff(this.semesterStart, 'week');
    const icsData = [];
    courseArray.forEach(course => {
      for (let i = 0; i < semesterLength + 1; ++i) {
        const date = moment(this.semesterStart.weekday(0)).format('YYYY, MM, DD,') + ' ' +
          course.hour.split('-')[0].split(':')[0] + ', ' +
          course.hour.split('-')[0].split(':')[1];
        const startDate = moment(date, 'YYYY, MM, DD, HH, mm');
        startDate.add(i, 'week');
        startDate.add(course.dayInd, 'day');
        const endDate = moment(startDate);
        endDate.add(course.duration, 'hour');
        const data = {
          summary: course.name,
          start: startDate,
          end: endDate,
          description: course.teacher,
          location: course.location
        };
        icsData.push(data);
      }
    });


    const cal = ical();
    cal.events(icsData);
    window.open(cal.toURL(), '_self');
  }

  private unboxCourses(courses: any[][]): Course[] {
    const courseArray: Course[] = [];
    for (let j = 0; j < 49; ++j) {
      for (let k = 1; k < 6; ++k) {
        if (courses[j][k].length === 1) {
          courseArray.push(courses[j][k][0]);
        } else {
          if (courses[j][k].length > 1) {
            courses[j][k].forEach(course => {
              courseArray.push(course);
            });
          }
        }
      }
    }
    return courseArray;
  }
}
