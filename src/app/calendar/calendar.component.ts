import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Course, User} from '../service/Interfaces';
import {DefaultService} from '../service/default.service';
import {CalendarService} from './service/calendar.service';
import {ExportService} from './service/export.service';
import * as moment from 'moment';
import {HttpService} from '../service/http.service';
import {MsAdalAngular6Service} from 'microsoft-adal-angular6';
import {UserService} from '../service/user.service';
import {timeInterval, timeout} from 'rxjs/operators';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  breakpoint = 4;
  screenWidth = window.innerWidth;
  coursePlannerButtonTitle = true;
  calendarPlannerButtonTitle = true;
  showCoursePlanner = true;
  showCalendarPlanner = true;
  courseButtonTitle = true;
  showCourse = true;
  calendarButtonTitle = true;
  showCalendar = true;
  searchWord = '';
  courseResults: Course[] = [];
  filteredCourseResults: Course[] = [];
  coursePlannerTable: any[] = [];
  courseFinalTable: any[] = [];
  finalCourses: Course[] = [];
  courseFinalCount = 0;
  activeSemester = '';
  filter = '';
  showZeroAdmin = true;
  lastSaveTime: string;
  user: User;
  view = '1';

  constructor(
    public calendarService: CalendarService,
    public defaultService: DefaultService,
    public exportService: ExportService,
    public httpService: HttpService,
    public adalSvc: MsAdalAngular6Service,
    public userService: UserService
  ) {
  }

  ngOnInit() {
    this.checkScreenWidth();
    this.resetCourseTable(this.coursePlannerTable);
    this.resetCourseTable(this.courseFinalTable);
    this.calendarService.getSemesters().subscribe(semesters => {
      this.activeSemester = this.getActiveSemester(semesters);
    });
    this.userService.getUserTimetable().subscribe( user => {
      if (typeof user[0] === 'undefined') {
        return;
      }
      this.view = user[0].view;
      this.lastSaveTime = user[0].lastChange;
    });
  }

  resetCourseTable(table: any[]) {
    const hours = this.calendarService.getHours();
    for (let i = 0; i < hours.length; ++i) {
      table[i] = [];
      table[i][0] = hours[i];
      for (let j = 1; j < 6; ++j) {
        table[i][j] = [];
      }
    }
  }

  onResize(event) {
    this.screenWidth = event.target.innerWidth;
    this.checkScreenWidth();
  }

  setCalendarView(newView: string) {
    this.httpService.putUserView(this.adalSvc.userInfo.userName, newView).subscribe();
  }

  searchForCourse(method: string) {
    this.filteredCourseResults = [];
    this.courseResults = [];
    this.resetCourseTable(this.coursePlannerTable);
    switch (method) {
      case 'name':
        this.calendarService.getCoursesByName(this.activeSemester, this.searchWord).subscribe(
          courses => {
            this.setCourseResults(courses[0]);
            this.populatePlannerTable();
            this.filter = '';
            this.showZeroAdmin = true;
          });
        break;
      case 'code':
        this.calendarService.getCoursesByCode(this.activeSemester, this.searchWord).subscribe(
          courses => {
            this.setCourseResults(courses[0]);
            this.populatePlannerTable();
            this.filter = '';
            this.showZeroAdmin = true;
          });
        break;
      case 'teacher':
        this.calendarService.getCoursesByTeacher(this.activeSemester, this.searchWord).subscribe(
          courses => {
            this.setCourseResults(courses[0]);
            this.populatePlannerTable();
            this.filter = '';
            this.showZeroAdmin = true;
          });
        break;
      case 'teacherCode':
        this.calendarService.getCoursesByTeacherCode(this.activeSemester, this.searchWord).subscribe(
          courses => {
            this.setCourseResults(courses[0]);
            this.populatePlannerTable();
            this.filter = '';
            this.showZeroAdmin = true;
          });
        break;
    }
  }

  setCourseResults(courses: any[]) {
    courses.forEach(
      course => {
        if (course.idopont) {
          const startHour = course.idopont.split(' ')[1].split('-')[0];
          const endHour = course.idopont.split(' ')[1].split('-')[1];
          const courseDay = course.idopont.split(' ')[0];
          const duration = moment(endHour, 'HH:mm').diff(moment(startHour, 'HH:mm'));
          const newCourse: Course = {
            code: course.kurzuskod,
            name: course.kurzusnev,
            course_code: course.csoport,
            type: course.oratipus,
            day: courseDay,
            dayInd: +course.nap,
            hour: course.idopont.split(' ')[1],
            location: course.helyszin,
            teacher: course.oktato,
            selected: false,
            duration: duration / (1000 * 60 * 60),
            zeroAdmin: course.oktato.includes('adminisztrátor'),
            level: this.calendarService.getHours().findIndex(data => data === course.idopont.split(' ')[1].split('-')[0])
          };
          this.courseResults.push(newCourse);
        }
      }
    );
  }

  populatePlannerTable() {
    const hours = this.calendarService.getHours();
    for (let i = 0; i < 49; ++i) {
      this.courseResults.forEach(data => {
        const hour = (data.hour.split('-')[0]);
        if (hour === hours[i]) {
          for (let j = 1; j < 6; j++) {
            if (data.dayInd === j) {
              this.coursePlannerTable[i][j].push(data);
            }
          }
        }
      });
    }
  }

  closePanel(panel: string) {
    switch (panel) {
      case 'coursePlanner':
        this.coursePlannerButtonTitle = !this.coursePlannerButtonTitle;
        this.showCoursePlanner = !this.showCoursePlanner;
        break;
      case 'calendarPlanner':
        this.calendarPlannerButtonTitle = !this.calendarPlannerButtonTitle;
        this.showCalendarPlanner = !this.showCalendarPlanner;
        break;
      case 'course':
        this.courseButtonTitle = !this.courseButtonTitle;
        this.showCourse = !this.showCourse;
        break;
      case 'calendar':
        this.calendarButtonTitle = !this.calendarButtonTitle;
        this.showCalendar = !this.showCalendar;
        break;
    }
  }

  setClickedRow(i: number, course: Course) {
    if (course.selected) {
      this.deleteFinalCourse(course);
    } else {
      this.addFinalCourse(course);
      course.selected = !course.selected;
    }
  }

  selectCoursePlan(course: Course) {
    const selectedCourse = this.courseResults.find(data => this.compareCourses(data, course));
    if (selectedCourse.selected) {
      this.deleteFinalCourse(selectedCourse);
    } else {
      this.addFinalCourse(course);
      selectedCourse.selected = !selectedCourse.selected;
    }
  }

  addFinalCourse(course: Course) {
    const selectedCourse: Course = {
      code: course.code,
      course_code: course.course_code,
      day: course.day,
      dayInd: course.dayInd,
      duration: course.duration,
      hour: course.hour,
      location: course.location,
      name: course.name,
      selected: course.selected,
      teacher: course.teacher,
      type: course.type,
      zeroAdmin: course.zeroAdmin,
      level: course.level
    };
    const hours = this.calendarService.getHours();
    if (!(this.finalCourses.find(data => this.compareCourses(data, selectedCourse)))) {
      this.finalCourses.push(selectedCourse);
      for (let i = 0; i < hours.length; ++i) {
        if (selectedCourse.hour.split('-')[0] === hours[i]) {
          for (let j = 1; j < 6; j++) {
            if (selectedCourse.dayInd === j) {
              this.courseFinalTable[i][j].push(selectedCourse);
              this.courseFinalCount++;
            }
          }
        }
      }
    }
  }

  deleteFinalCourse(course: Course) {
    const finalCourseIndex = this.finalCourses.findIndex(data => this.compareCourses(data, course));
    const coursePlan = this.courseResults.find(data => this.compareCourses(data, course));
    if (!!coursePlan) {
      coursePlan.selected = !coursePlan.selected;
    }
    this.finalCourses.splice(finalCourseIndex, 1);
    this.courseFinalTable.forEach(rows => {
      rows.forEach(row => {
        if (Array.isArray(row)) {
          row.forEach((rowCourse, index) => {
            if (this.compareCourses(rowCourse, course)) {
              row.splice(index, 1);
              this.courseFinalCount--;
            }
          });
        }
      });
    });
  }

  isArray(obj: any) {
    return Array.isArray(obj);
  }

  getActiveSemester(semesters: string[]): string {
    const date = moment();
    let ret = '';
    semesters.forEach(semester => {
      const dateValue = Object.values(semester)[0];
      const dateFirstYear = dateValue.split('-')[0];
      const dateSecondYear = dateValue.split('-')[1];
      const dateSemester = dateValue.split('-')[2];
      if ((date.month() >= 0 && date.month() <= 6) && dateSemester === '2' && dateSecondYear === date.format('YYYY')) {
        ret = dateValue;
      } else {
        if ((date.month() > 6 && date.month() <= 11) && dateSemester === '1' && dateFirstYear === date.format('YYYY')) {
          ret = dateValue;
        }
      }
    });
    return ret;
  }

  compareCourses(objThis: Course, objThat: Course) {
    return (objThis.code === objThat.code &&
      objThis.name === objThat.name &&
      objThis.course_code === objThat.course_code &&
      objThis.type === objThat.type &&
      objThis.day === objThat.day &&
      objThis.dayInd === objThat.dayInd &&
      objThis.hour === objThat.hour &&
      objThis.location === objThat.location &&
      objThis.teacher === objThat.teacher &&
      objThis.duration === objThat.duration &&
      objThis.zeroAdmin === objThat.zeroAdmin);
  }

  print() {
    window.print();
  }

  saveTimetable() {
    const saveArray = [];
    this.courseFinalTable.forEach(row => {
        row.forEach(course => {
            if (this.isArray(course) && course.length > 0) {
              course.forEach(data => {
                  saveArray.push(data);
                }
              );
            }
          }
        );
      }
    );
    const calendarChange = {
      timetable: JSON.stringify(saveArray),
      lastChange: moment().format('YYYY-MM-DD HH:mm:ss')
    };
    this.httpService.putUserCalendar(this.adalSvc.userInfo.userName, calendarChange).subscribe();
    this.lastSaveTime = calendarChange.lastChange;
  }

  loadTimetable() {
    this.userService.getUserTimetable().subscribe(user => {
      if (user[0].timetable === null) {
        return;
      }
      (JSON.parse(user[0].timetable)).forEach(
        course => {
          this.addFinalCourse(course);
        }
      );
    });

  }

  private checkScreenWidth() {
    this.breakpoint = this.screenWidth < 768 ? 2 : 4;
  }
}
