import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'listFilter'
})
export class ListFilterPipe implements PipeTransform {

  transform(list: any[], value: string, except: boolean) {

    if(!except){
      return value ? list.filter(item => item.type === value && !item.zeroAdmin) : list.filter(item => !item.zeroAdmin);
    }else{
      return value ? list.filter(item => item.type === value) : list;
    }

  }

}
