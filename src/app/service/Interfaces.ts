export interface Major {
  id: number;
  name: string;
  degree: string;
  short_name: string;
  year: number;
}

export interface Subject {
  code: number;
  name: string;
  type: string;
  exam: string;
  exercise_mark: string;
  credit: number;
  prerequisite1: string;
  prerequisite2: string;
  recommended: string;
  required_optional: number;
}

export interface Course {
  code: string;
  name: string;
  course_code: number;
  type: string;
  day: string;
  dayInd: number;
  hour: string;
  location: string;
  teacher: string;
  selected: boolean;
  duration: number;
  zeroAdmin: boolean;
  level: number;
}

export interface Day {
  ind: number;
  value: string;
}

export interface Bug {
  name: string;
  userName: string;
  text: string;
}

export interface User {
  name: string;
  userName: string;
  view: string;
  timetable: string;
  lastChange: string;
}
