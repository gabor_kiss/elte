import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {MsAdalAngular6Service} from 'microsoft-adal-angular6';
import {HttpService} from './http.service';
import {User} from './Interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;

  constructor(
    public adalSvc: MsAdalAngular6Service,
    public httpService: HttpService
  ) {

  }

  getUserTimetable(): Observable<User> {
    return this.httpService.getUser(this.adalSvc.userInfo.userName);
  }

}
