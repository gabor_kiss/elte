import { TestBed } from '@angular/core/testing';

import { DefaultService } from './default.service';
import {MatSnackBarModule} from '@angular/material';

describe('DefaultService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MatSnackBarModule
    ]
  }));

  it('should be created', () => {
    const service: DefaultService = TestBed.get(DefaultService);
    expect(service).toBeTruthy();
  });
});
