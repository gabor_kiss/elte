import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Bug, Major, Subject, User} from './Interfaces';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) {
  }

  getAllMajors() {
    return this.http.get<Major[]>('/api/majors');
  }

  getSubjectsByMajorIdAndYear(majorId: number, year: number) {
    return this.http.get<Subject[]>('/api/subjects/' + majorId + '/' + year );
  }

  getAllSemesters() {
    return this.http.get<string[]>('/api/semesters');
  }

  getCoursesByName(semester: string, courseName: string) {
    return this.http.get<any[]>('/api/courses/courseName/' + semester + '/' + courseName );
  }

  getCoursesByCode(semester: string, code: string) {
    return this.http.get<any[]>('/api/courses/code/' + semester + '/' + code );
  }

  getCoursesByTeacher(semester: string, teacher: string) {
    return this.http.get<any[]>('/api/courses/teacher/' + semester + '/' + teacher );
  }

  getCoursesByTeacherCode(semester: string, teacherCode: string) {
    return this.http.get<any[]>('/api/courses/teacherCode/' + semester + '/' + teacherCode );
  }

  postBug(bug: Bug) {
    return this.http.post<Bug>('/api/bug', bug);
  }

  getUser(userName: string): Observable<User> {
    return this.http.get<User>('/api/users/' + userName);
  }

  postUser(user: User) {
    return this.http.post<User>('/api/users/', user);
  }

  putUserView(userName: string, view: any) {
    console.log(view);
    return this.http.put<User>('/api/users/view/' + userName, {data: view});
  }

  putUserCalendar(userName: string, calendarChange: any) {
    return this.http.put<User>('/api/users/calendar/' + userName, calendarChange);
  }

}
