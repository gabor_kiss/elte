'use strict';
module.exports = function (app) {
  const subjectList = require('../server/appController');
  const majorList = require('../server/appController');
  const semesterList = require('../server/appController');
  const courseList = require('../server/appController');
  const bug = require('../server/appController');
  const user = require('../server/appController');

  app.route('/api/subjects/:majorId/:recommended')
    .get(subjectList.list_subjects_by_major_and_recommended);

  app.route('/api/majors')
    .get(majorList.list_all_majors);

  app.route('/api/semesters')
    .get(semesterList.list_all_semesters);

  app.route('/api/courses/courseName/:semester/:courseName')
    .get(courseList.list_all_courses_by_name);

  app.route('/api/courses/code/:semester/:code')
    .get(courseList.list_all_courses_by_code);

  app.route('/api/courses/teacher/:semester/:teacher')
    .get(courseList.list_all_courses_by_teacher);

  app.route('/api/courses/teacherCode/:semester/:teacherCode')
    .get(courseList.list_all_courses_by_teacherCode);

  app.route('/api/bug')
    .post(bug.postBug);

  app.route('/api/users/:userName')
    .get(user.getUser);

  app.route('/api/users/view/:userName')
    .put(user.putUserView);

  app.route('/api/users/calendar/:userName')
    .put(user.putUserCalendar);

  app.route('/api/users')
    .post(user.postUser)
};
