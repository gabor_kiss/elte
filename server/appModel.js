'use strict';
const jawsDb = require('../db/jawsdb');
const ttkDb = require('../db/ttkdb');

const Subject = function (subject) {
};
const Major = function (major) {
};
const Course = function (course) {
};
const Semester = function (semester) {
};
const Bug = function (bug) {
};
const User = function (user) {
};

Subject.getSubjectsByMajorAndRecommended = function (major_id, recommended, result) {
  jawsDb.query('select * from v3gf34th7x0405fh.subject where major_id = ? and INSTR(recommended, ?)>0', [major_id, recommended], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Major.getAllMajors = function (result) {
  jawsDb.query('select * from v3gf34th7x0405fh.major', function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Semester.getAllSemesters = function (result) {
  ttkDb.then(function (connection) {
    connection.query('select tanev_felev from teszt.felevek', function (err, res) {

      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  });
};

Course.getAllCoursesByName = function (semester, courseName, result) {
  ttkDb.then(function (connection) {
    connection.query('call web_targynev(?,?,\'1000\')', [semester, courseName], function (err, res) {

      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  });
};

Course.getAllCoursesByCode = function (semester, code, result) {
  ttkDb.then(function (connection) {
    connection.query('call web_targykod(?,?,\'1000\')', [semester, code], function (err, res) {

      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  });

};

Course.getAllCoursesByTeacher = function (semester, teacher, result) {
  ttkDb.then(function (connection) {
    connection.query(`call web_oktnev(?,?,\'1000\')`, [semester, teacher], function (err, res) {

      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  });
};

Course.getAllCoursesByTeacherCode = function (semester, teacherCode, result) {
  ttkDb.then(function (connection) {
    connection.query('call web_oktneptun(?,?,\'1000\')', [semester, teacherCode], function (err, res) {

      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  });
};

Bug.postBug = function (bug, result) {
  jawsDb.query('insert into bug (name, userName, text, date) values (?, ?, ?, ?)', [bug.name, bug.userName, bug.text, bug.date], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

User.postUser = function (user, result) {
  console.log(user);
  jawsDb.query('insert into user (name, userName, view) values (?, ?, ?)', [user.name, user.userName, user.view], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

User.getUser = function (userName, result) {
  jawsDb.query('select * from user where userName = ?', [userName], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

User.putUserView = function (userName, body, result) {
  jawsDb.query('update user set user.view = ? where userName = ?', [body.data, userName], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

User.putUserCalendar = function (userName, body, result) {
  jawsDb.query('update user set user.timetable = ?, user.lastChange = ? where userName = ?', [body.timetable, body.lastChange, userName], function (err, res) {

    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};


module.exports = {
  Major, Subject, Course, Semester, Bug, User
};
