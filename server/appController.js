'use strict'
const jwt = require('jsonwebtoken');

const jwksClient = require('jwks-rsa');
const client = jwksClient({
  jwksUri: 'https://login.microsoftonline.com/common/discovery/keys'
});

function getKey(header, callback) {
  client.getSigningKey(header.kid, function (err, key) {
    const signingKey = key.publicKey || key.rsaPublicKey;
    callback(null, signingKey);
  });
}

const Methods = require('../server/appModel');

exports.list_subjects_by_major_and_recommended = function (req, res) {
  Methods.Subject.getSubjectsByMajorAndRecommended(req.params.majorId, req.params.recommended, function (err, subjects) {
    if (err)
      res.send(err);
    res.json(subjects);
  });
};

exports.list_all_majors = function (req, res) {
  Methods.Major.getAllMajors(function (err, majors) {
    if (err)
      res.send(err);
    res.json(majors);
  })
};

exports.list_all_semesters = function (req, res) {
  Methods.Semester.getAllSemesters(function (err, semesters) {
    if (err)
      res.send(err);
    res.json(semesters);
  })
};

exports.list_all_courses_by_name = function (req, res) {
  Methods.Course.getAllCoursesByName(req.params.semester, req.params.courseName, function (err, courses) {
    if (err)
      res.send(err);
    res.json(courses);
  })
};

exports.list_all_courses_by_code = function (req, res) {
  Methods.Course.getAllCoursesByCode(req.params.semester, req.params.code, function (err, courses) {
    if (err)
      res.send(err);
    res.json(courses);
  })
};

exports.list_all_courses_by_teacher = function (req, res) {
  Methods.Course.getAllCoursesByTeacher(req.params.semester, req.params.teacher, function (err, courses) {
    if (err)
      res.send(err);
    res.json(courses);
  })
};

exports.list_all_courses_by_teacherCode = function (req, res) {
  Methods.Course.getAllCoursesByTeacherCode(req.params.semester, req.params.teacherCode, function (err, courses) {
    if (err)
      res.send(err);
    res.json(courses);
  })
};

exports.postBug = function (req, res) {
  Methods.Bug.postBug(req.body, function (err, bug) {
    if (err)
      res.send(err);
    res.json(bug);
  })
};

exports.getUser = function (req, res) {
  let token = req.headers.authorization.slice(7, req.headers.authorization.length);
  jwt.verify(token, getKey, {algorithms: ['RS256']}, function (err, decoded) {
    if (!!decoded && decoded.unique_name === req.params.userName) {
      Methods.User.getUser(req.params.userName, function (err, user) {
        if (err)
          res.send(err);
        res.json(user);
      })
    } else {
      res.status(400).send();
    }
  });
};

exports.postUser = function (req, res) {
  let token = req.headers.authorization.slice(7, req.headers.authorization.length);
  jwt.verify(token, getKey, {algorithms: ['RS256']}, function (err, decoded) {
    Methods.User.postUser(req.body, function (err, user) {
      if (err)
        res.send(err);
      res.json(user);
    })
  });
};

exports.putUserView = function (req, res) {
  let token = req.headers.authorization.slice(7, req.headers.authorization.length);
  jwt.verify(token, getKey, {algorithms: ['RS256']}, function (err, decoded) {
    if (!!decoded && decoded.unique_name === req.params.userName) {
      Methods.User.putUserView(req.params.userName, req.body, function (err, user) {
        if (err)
          res.send(err);
        res.json(user);
      })
    } else {
      res.status(400).send();
    }
  });
};

exports.putUserCalendar = function (req, res) {
  let token = req.headers.authorization.slice(7, req.headers.authorization.length);
  jwt.verify(token, getKey, {algorithms: ['RS256']}, function (err, decoded) {
    if (!!decoded && decoded.unique_name === req.params.userName) {
      Methods.User.putUserCalendar(req.params.userName, req.body, function (err, user) {
        if (err)
          res.send(err);
        res.json(user);
      })
    } else {
      res.status(400).send();
    }
  });
};

